import * as httpClient from "agilesoft-http";
import { VisualComponentsConfiguration, VisualComponentsRequest, VisualComponentsResponse } from "./visual-components.interfaces";
export declare const configuration: (configurations: VisualComponentsConfiguration) => void;
export declare const getVisualComponents: (request: VisualComponentsRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<VisualComponentsResponse>>;
