export interface VisualComponentsConfiguration {
    host: string;
    timeout: number;
}
export interface VisualComponentsRequest {
    module: string;
    type: string;
    group: string;
}
export interface VisualComponentsResponse {
    data: Components[];
}
export interface Components {
    _id: string;
    groupCode: string;
    moduleID: string;
    section: string;
    name: string;
    type: string;
    content: string;
    __v: number;
}
