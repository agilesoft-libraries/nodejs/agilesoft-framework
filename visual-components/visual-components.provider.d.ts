import * as httpClient from "agilesoft-http";
import { VisualComponentsRequest, VisualComponentsResponse } from "./visual-components.interfaces";
export declare class VisualComponentsFrameworkProvider {
    obtenerComponenteVisual(request: VisualComponentsRequest, extra?: httpClient.ExtraOptions): Promise<httpClient.ResponseData<VisualComponentsResponse>>;
}
