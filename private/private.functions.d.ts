import * as httpClient from "agilesoft-http";
import { PrivateConfiguration, LoginIntentionRequest, LoginIntentionResponse, PrivateStorageRequest, PrivateStorageResponse, PrivateStorageEntityRequest } from "./private.interfaces";
export declare const configuration: (configurations: PrivateConfiguration) => void;
export declare const loginIntention: (request: LoginIntentionRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<LoginIntentionResponse>>;
export declare const createOrUpdateSessionStorage: (request: PrivateStorageRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<PrivateStorageResponse<boolean>>>;
export declare const getSessionStorage: <T>(request: PrivateStorageEntityRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<PrivateStorageResponse<T>>>;
export declare const deleteSessionStorage: <T>(request: PrivateStorageEntityRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<PrivateStorageResponse<boolean>>>;
