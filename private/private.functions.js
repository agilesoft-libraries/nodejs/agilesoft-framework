"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSessionStorage = exports.getSessionStorage = exports.createOrUpdateSessionStorage = exports.loginIntention = exports.configuration = void 0;
const httpClient = __importStar(require("agilesoft-http"));
let config;
const configuration = (configurations) => {
    config = configurations;
};
exports.configuration = configuration;
const createBearerHeaders = (jwtSession) => {
    return {
        Authorization: `Bearer ${jwtSession}`,
    };
};
const loginIntention = (request, extra) => __awaiter(void 0, void 0, void 0, function* () {
    return httpClient.doRequest({
        url: `${config.host}/api/private/v1/loginintention`,
        method: httpClient.Method.POST,
        data: request,
        options: { timeout: config.timeout },
    }, extra);
});
exports.loginIntention = loginIntention;
const createOrUpdateSessionStorage = (request, extra) => __awaiter(void 0, void 0, void 0, function* () {
    return httpClient.doRequest({
        url: `${config.host}/api/private/v1/session/storage`,
        method: httpClient.Method.POST,
        data: {
            storageKey: request.storageKey,
            storageValue: request.storageValue,
        },
        trxId: request.storageKey,
        options: { timeout: config.timeout },
        headers: createBearerHeaders(request.jwtSession),
    }, extra);
});
exports.createOrUpdateSessionStorage = createOrUpdateSessionStorage;
const getSessionStorage = (request, extra) => __awaiter(void 0, void 0, void 0, function* () {
    return httpClient.doRequest({
        url: `${config.host}/api/private/v1/session/storage/${request.storageKey}`,
        trxId: request.storageKey,
        options: { timeout: config.timeout },
        headers: createBearerHeaders(request.jwtSession),
    }, extra);
});
exports.getSessionStorage = getSessionStorage;
const deleteSessionStorage = (request, extra) => __awaiter(void 0, void 0, void 0, function* () {
    return httpClient.doRequest({
        url: `${config.host}/api/private/v1/session/storage/${request.storageKey}`,
        method: httpClient.Method.PUT,
        trxId: request.storageKey,
        options: { timeout: config.timeout },
        headers: createBearerHeaders(request.jwtSession),
    }, extra);
});
exports.deleteSessionStorage = deleteSessionStorage;
//# sourceMappingURL=private.functions.js.map