import * as httpClient from "agilesoft-http";
import { LoginIntentionRequest, LoginIntentionResponse } from "./private.interfaces";
export declare class PrivateFrameworkProvider {
    loginIntention(request: LoginIntentionRequest, extra?: httpClient.ExtraOptions): Promise<httpClient.ResponseData<LoginIntentionResponse>>;
}
