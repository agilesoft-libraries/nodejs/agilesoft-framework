export interface PrivateConfiguration {
    host: string;
    timeout: number;
}
export interface LoginIntentionRequest {
    context: string;
    username: string;
    roles: string[];
    dataKey: string;
    data: any;
}
export interface LoginIntentionResponse {
    data: string;
}
export interface PrivateStorageRequest {
    storageKey: string;
    storageValue: string;
    jwtSession: string;
}
export interface PrivateStorageResponse<T> {
    data: T;
}
export interface PrivateStorageEntityRequest {
    storageKey: string;
    jwtSession: string;
}
