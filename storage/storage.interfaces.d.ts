export interface StorageConfiguration {
    host: string;
    timeout: number;
}
export interface StorageRequest {
    key: string;
    jwtSession: string;
}
export interface StorageEntityRequest {
    key: string;
    value: any;
    jwtSession: string;
}
export interface StorageResponse<T> {
    data: T;
}
