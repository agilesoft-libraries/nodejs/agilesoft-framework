import * as httpClient from "agilesoft-http";
import { StorageConfiguration, StorageRequest, StorageResponse, StorageEntityRequest } from "./storage.interfaces";
export declare const configuration: (configurations: StorageConfiguration) => void;
export declare const getStorage: <T>(request: StorageRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<StorageResponse<T>>>;
export declare const createOrUpdateStorage: (request: StorageEntityRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<StorageResponse<boolean>>>;
export declare const deleteStorage: (request: StorageRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<StorageResponse<boolean>>>;
