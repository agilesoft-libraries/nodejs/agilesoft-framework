import * as httpClient from "agilesoft-http";
import { StorageRequest, StorageEntityRequest, StorageResponse } from "./storage.interfaces";
export declare class StorageFrameworkProvider {
    getStorage<T>(request: StorageRequest, extra?: httpClient.ExtraOptions): Promise<httpClient.ResponseData<StorageResponse<T>>>;
    createOrUpdateStorage(request: StorageEntityRequest, extra?: httpClient.ExtraOptions): Promise<httpClient.ResponseData<StorageResponse<boolean>>>;
    deleteStorage(request: StorageRequest, extra?: httpClient.ExtraOptions): Promise<httpClient.ResponseData<StorageResponse<boolean>>>;
}
