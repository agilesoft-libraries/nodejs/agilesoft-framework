export interface ParameterConfiguration {
    host: string;
    timeout: number;
}
export interface IParameter {
    name: string;
    value: string;
    type: string;
}
export interface IParameterResponse {
    data: IParameter;
}
export interface IParameterRequest {
    moduleID: string;
    parameterName: string;
    group: string;
}
