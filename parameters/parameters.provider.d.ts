import * as httpClient from "agilesoft-http";
import { IParameterRequest, IParameterResponse } from "./parameters.interfaces";
export declare class ParameterFrameworkProvider {
    getParameter(paramRequest: IParameterRequest, extra?: httpClient.ExtraOptions): Promise<httpClient.ResponseData<IParameterResponse>>;
}
