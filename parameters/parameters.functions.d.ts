import * as httpClient from "agilesoft-http";
import { IParameterRequest, IParameterResponse, ParameterConfiguration } from "./parameters.interfaces";
export declare const configuration: (configurations: ParameterConfiguration) => void;
export declare const getParameter: (paramRequest: IParameterRequest, extra?: httpClient.ExtraOptions | undefined) => Promise<httpClient.ResponseData<IParameterResponse>>;
